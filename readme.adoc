# OSGi an example of implementation

Simple Hello World! application

## Details of modules
The application contains two modules

<1> service
<2> client

### Service
Service is able to return a processed String

### Client
Client is able to monitor the registration of service
and to request the service procedure once it is registered

## Implementation
There is an interface which is called like Greeter

[source:java]
----
include::service/src/main/java/ua/odessa/bondary/osgi/service/definition/Greeter.java[inteface=public]
----

Add an implementation of this interface Service module in class GreeterImpl
[source::java,linenums]
----
include::service/src/main/java/ua/odessa/bondary/osgi/service/implementation/GreeterImpl.java[lines=30..33]
----

The OSGi felix API provides Interfaces to implement them helps us adopt our code to execute in OSGI environment
one of them is `BundleActivator`

The implementations for required an interface methods were added into GreeterImpl:
[source:java]
----
include::service/src/main/java/ua/odessa/bondary/osgi/service/implementation/GreeterImpl.java[lines=13;14;17..28]
----


=== From Client side the behavior as follows
===== Client implements `BundleActivitor` interface there two methods `start` and `stop` accordingly.
During Start method execution register listener when required service to registered
[source::java]
----
include::client/src/main/java/ua/odessa/bondary/osgi/Client.java[lines=8;11..15]
----
===== This registered listener will help to generate an event when the service registration is finished.
To catch up this listener events `ServiceListener` interface should be implemented.
This interface contains one methos `serviceChanged(ServiceEvent serviceEvent)`
[source:java]
----
include::client/src/main/java/ua/odessa/bondary/osgi/Client.java[lines=22..23]
----

This method contains the switch to filter a required event.
[source:java]
----
include::client/src/main/java/ua/odessa/bondary/osgi/Client.java[lines=22..45]
----
===== Once Service is registered the listener will call the method serviceChanged and client side code is able to get a reference to a registered service bean. And as result to call a method of this bean
[source:java]
----
include::client/src/main/java/ua/odessa/bondary/osgi/Client.java[lines=30..32]
----

=== Development setup and Debug this code


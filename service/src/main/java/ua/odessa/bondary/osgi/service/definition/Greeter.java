package ua.odessa.bondary.osgi.service.definition;

public interface Greeter {
    String sayHiTo(String name);
}

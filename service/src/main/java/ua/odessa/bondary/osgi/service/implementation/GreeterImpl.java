package ua.odessa.bondary.osgi.service.implementation;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import ua.odessa.bondary.osgi.service.definition.Greeter;

import java.util.Hashtable;

public class GreeterImpl implements Greeter, BundleActivator {

    private ServiceReference<Greeter> reference;
    private ServiceRegistration<Greeter> registration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        System.out.println("Register service.");
        registration = bundleContext.registerService(Greeter.class, new GreeterImpl(),new Hashtable<>());
        reference = registration.getReference();

    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        System.out.println("Unregistering service.");
        registration.unregister();
    }

    @Override
    public String sayHiTo(String name) {
        return "Hello "+name;
    }
}

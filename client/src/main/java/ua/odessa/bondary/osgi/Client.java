package ua.odessa.bondary.osgi;

import org.osgi.framework.*;
import ua.odessa.bondary.osgi.service.definition.Greeter;

public class Client implements BundleActivator, ServiceListener {

    private BundleContext context;
    private ServiceReference reference;

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        this.context = bundleContext;
        context.addServiceListener(this,"(objectclass=" + Greeter.class.getName() + ")");
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {

    }

    @Override
    public void serviceChanged(ServiceEvent serviceEvent) {
        int type = serviceEvent.getType();
        switch (type){
            case(ServiceEvent.REGISTERED):
                System.out.println("Notification of service registered.");
                reference = serviceEvent
                        .getServiceReference();
                if (context.getService(reference) instanceof Greeter) {
                    Greeter service = (Greeter) (context.getService(reference));
                    System.out.println( service.sayHiTo("Yurii") );
                } else {
                    System.out.println("Something is went wrong because we have registered to Greeter class' event !!!");
                }
                break;
            case(ServiceEvent.UNREGISTERING):
                System.out.println("Notification of service unregistered.");
                context.ungetService(serviceEvent.getServiceReference());
                break;
            default:
                break;
        }

    }
}
